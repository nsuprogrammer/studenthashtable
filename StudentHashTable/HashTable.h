#pragma once
//class HashTable
//{
//public:
//	HashTable();
//	~HashTable();
//};

#include <string.h>
#include <iostream>
//#include <conio.h>
#include <string>
#include <sstream> // toString 
#include <stddef.h>

#define DEBUG 1


using namespace ::std;
typedef std::string Key; //�������������� ���� ������ � ���

unsigned int HashFunction(Key b, unsigned int s);

///////////////////////////////////////////////////////////////////////////////

struct Value {
	unsigned int age;
	unsigned int weight;
	//
	static unsigned int id;

	Value() { age = id; weight = id; id++; }
	//Value() : age(18), weight(80) { id++; }
	Value(unsigned int age, unsigned int weight) : age(age), weight(weight) {}
	bool operator==(const Value &a) const;
};

unsigned int Value::id = 0;

bool Value::operator==(const Value &b) const
{
	if ((this->age == b.age) && (this->weight == b.weight)) return true;
	else
		return false;
};

std::ostream &operator<<(std::ostream &os, Value const &value) {
	//return os << "Value[age= " << value.age << " , weight= " << value.weight << " ]";
	return os << "Value[memAdr=" << &value << "] [age= " << value.age << " , weight= " << value.weight << " ]";
}

///////////////////////////////////////////////////////////////////////////////

struct Item {
	Key Keys; // focus
	Value Student; 
	Item* Next;

	Item() : Student(19, 90), Next(NULL) { DEBUG && cout << "����������� " << toString() << "\n"; };
	Item(unsigned int age, unsigned int weight) : Student(age, weight), Next(NULL) { DEBUG && cout << "����������� " << toString() << "\n"; };
	Item(unsigned int age, unsigned int weight, Item* nextItem) : Student(age, weight), Next(nextItem) { DEBUG && cout << "����������� " << toString() << "\n"; };
	//Item(Item* nextItem) : Next(nextItem) { DEBUG && cout << "����������� " << toString() << "\n"; };
	/*Item(const Item& b) {
		DEBUG && cout << "����������� ����� Item " << toString() << "\n";
	}*/
	~Item()
	{
		DEBUG && cout << "���������� " << toString(this) << "\n";
	}

	
	// ������� ������� ��������� �������
	static bool recursiveDeleteItemList(Item* ptrItem)
	{
		if (ptrItem == NULL)
			return false;

		Item* ptrNextItem;
		do
		{
			ptrNextItem = ptrItem->Next;
			delete ptrItem;
			ptrItem = ptrNextItem;
		} while (ptrItem != NULL);

		return true;
	}


	// �������������� � ������ 
	static std::string toString(Item* ptrItem)
	{
		std::stringstream buffer;
		std::string valueNext = ptrItem->Next == NULL ? " NULL" : " Item->";
		buffer << "Item[memAdr=" << ptrItem << "] " << ptrItem->Student << " keys= " << ptrItem->Keys << " next= " << valueNext; // ����� ������� ����� �������� ���� ����� 
		return buffer.str();
	}
	// �������������� � ������ 
	std::string toString()
	{
		return toString(this);
	}
};



std::ostream &operator<<(std::ostream &os, Item &item) {
	Item* ptrHelp = &(item);
	if (ptrHelp == NULL)
	{
		//cout << "error";
		return os << "Item[memAdr=NULL] ";
	}

	std::string offset = " ";
	do
	{
		os << Item::toString(ptrHelp) << "\n" << offset;
		ptrHelp = ptrHelp->Next;
		offset += " "; // �� ��������������� !?
	} while (ptrHelp != NULL);
	return os;
}

///////////////////////////////////////////////////////////////////////////////

class HashTable
{
public:
	// ����� ����� ��������� ����, �� ���� ���� ������ getX, setX 
	Item* Table = NULL;
	Item** ptrItemTable = NULL;
	unsigned int size = 0;
	unsigned int INIT_SIZE = 20;


	//�����������
	HashTable() {
		DEBUG && cout << "����������� HashTable\n";
		this->size = INIT_SIZE;
		ptrItemTable = new Item*[INIT_SIZE];
		for (int i = 0; i < this->size; i++)
		{
			ptrItemTable[i] = NULL;
		}

	};

	//����������
	~HashTable() {
		DEBUG && cout << "���������� HashTable\n";

		if (this->size != 0) {
			unsigned int i;
			for (i = 0; i <= this->size; i++)
			{
				Item HelpNow = this->Table[i];
				while (HelpNow.Next != NULL)
				{
					Item Help = *HelpNow.Next;
					delete HelpNow.Next;
					HelpNow = Help;
				}
			}
			delete this->Table;
		}
	};

	//����������� �����
	HashTable(const HashTable& b) {
		DEBUG && cout << "����������� ����� HashTable\n";
		Item *TableCopy;
		TableCopy = new Item[this->size];
		unsigned int i;
		for (i = 0; i <= this->size; i++) {
			TableCopy[i] = this->Table[i];
			if (this->Table[i].Next != NULL) {
				Item Help = *(this->Table[i].Next);
				while (Help.Next != NULL) {
					Help.Next = new Item;
					Help = *(Help.Next);
				}
			}
		}
	};

	//���������� �������� ���� ���-������
	void swap(HashTable& b) {
		DEBUG && cout << "swap HashTable\n";
		HashTable *PtrTable = &b; //���� �������� �� ����������
		Item Help;
		unsigned int j;
		if ((this->size) == (PtrTable->size)) {
			for (j = 0; j <= (this->size); j++)
			{
				if ((this->Table[j].Next == PtrTable->Table[j].Next) && (this->Table[j].Next != NULL)) {
					while (this->Table[j].Next != NULL)
					{
						if (Help.Next == PtrTable->Table[j].Next) {
							Help = this->Table[j];
							this->Table[j] = PtrTable->Table[j];
							PtrTable->Table[j] = Help;
						}
						else
							cout << "I can't do it. Size is not evenly size." << endl;
					}
				}
				else
					cout << "I can't do it. Size is not evenly size" << "\n";
				Help = PtrTable->Table[j];
				PtrTable->Table[j] = this->Table[j];
				this->Table[j] = Help;
			}
		}
		else
			cout << "I can't do it. Size is not evenly size" << "\n";
	};

	//������������ �������� ����� ������
	void operator=(const HashTable&b) {//���� ������ ��������
		DEBUG && cout << "operator= HashTable\n";
		const HashTable *PtrTable = &b;
		unsigned int j;
		if ((this->size) == (PtrTable->size)) {
			for (j = 0; j <= (this->size); j++) {
				this->Table[j] = PtrTable->Table[j];
				if ((this->Table[j].Next != NULL) && (PtrTable->Table[j].Next != NULL)) {
					Item *HelpThis, *HelpTable;
					HelpThis = this->Table[j].Next;
					HelpTable = PtrTable->Table[j].Next;
					while ((HelpThis->Next != NULL) || (HelpTable->Next != NULL)) {
						if ((HelpThis->Next != NULL) && (HelpTable->Next != NULL)) {
							Item *Help;
							Help = HelpThis;
							HelpThis = HelpTable;
							HelpTable = Help;
							delete Help;
							HelpThis = HelpThis->Next;
							HelpTable = HelpTable->Next;
						}
						else
							cout << "I can't do it. Size is not evenly size";
					}
				}
			}
		}
		else
			cout << "I can't do it. Size is not evenly size" << "\n";
	};
	//������� ���������
	void clear() {
		DEBUG && cout << "clear HashTable\n";
		unsigned int j;
		for (j = 0; j <= (this->size); j++) {
			if (this->Table[j].Next != NULL) {
				Item *Help, *HelpNow;
				HelpNow = Table[j].Next;
				Help = HelpNow->Next;
				while (Help != NULL) {
					delete HelpNow;
					HelpNow = Help;
					Help = Help->Next;
				}
				delete Help;
				delete HelpNow;
			}
			this->Table[j].Keys = "";
			this->Table[j].Next = NULL;
			this->Table[j].Student = { 0, 0 };
		}
	};
	//������� ������� �� ��������� �����
	bool erase(const Key& k) {//bool ���������� true ��� false
		DEBUG && cout << "erase HashTable\n";
		unsigned int Hash;
		Hash = HashFunction(k, this->size);
		if (this->Table[Hash].Keys == k) {
			if (this->Table[Hash].Next == NULL) {
				this->Table[Hash].Keys = "";
				this->Table[Hash].Next = NULL;
				this->Table[Hash].Student = { 0, 0 };
				return true;
			}
			else {
				Item Help;
				Help = *(this->Table[Hash].Next);
				this->Table[Hash] = Help;
				while
					(Help.Next != NULL)
					Help = *(Help.Next);
				delete Help.Next;
				return true;
			}
		}
		else
		{
			if (this->Table[Hash].Next != NULL) {
				Item Help = *(this->Table[Hash].Next);
				while (Help.Next != NULL)
					if (Help.Keys == k)
					{
						Item Help1;
						Help1 = *(Help.Next);
						Help = Help1;
						while (Help1.Next != NULL)
							Help1 = *(Help1.Next);
						delete Help.Next;
						return true;
					}
				return false;
			}
			else
				return false;
		}
	};

	//-------------------------------------------------------------------------

	unsigned int hiredNumber()
	{
		unsigned int hired = 0;

		for (int j = 0; j <= this->size; j++)
		{
			if ((this->Table[j].Keys) != "") 
				hired++;
		}
		return hired;
	}

	bool hiredMoreCenter()
	{
		unsigned int hired = hiredNumber();
		unsigned int center = (this->size) / 2;
		
		if (hired >= center)
			return true;
		else
			return false;
	}

	//������� � ���������. ������������ �������� - ���������� �������
	bool insert(const Key& k, const Value& v) {
		DEBUG && cout << "insert HashTable\n";

		//unsigned int j;
		//unsigned int center = (this->size) / 2;
		//unsigned int hired = 0;
		unsigned int hash;

		hash = HashFunction(k, this->size);
		cout << hash << " hash\n";

		////�������� ������
		//if (this->size != 0) {
		//	for (j = 0; j <= this->size; j++)
		//	{
		//		if ((this->Table[j].Keys) != "") hired++;
		//	}
		//	if (hired >= center) {
		//		Item *HelpTable;
		//		HelpTable = new Item[this->size * 2];
		//		unsigned int k;
		//		for (k = 0; k <= (this->size); k++) {
		//			HelpTable[k].Student = this->Table[k].Student;
		//			HelpTable[k].Keys = this->Table[k].Keys;
		//			HelpTable[k].Next = this->Table[k].Next;
		//		}
		//		delete this->Table;
		//		this->size = this->size * 2;
		//		this->Table = new Item[this->size];
		//		for (k = 0; k <= (this->size) / 2; k++) {
		//			Hash = HashFunction(HelpTable[k].Keys, this->size);
		//			this->Table[Hash].Student = HelpTable[k].Student;
		//			this->Table[Hash].Keys = HelpTable[k].Keys;
		//			this->Table[Hash].Next = HelpTable[k].Next;
		//		}
		//		delete HelpTable;
		//	}
		//	//���������� �������� � ������� � ���������
		//	Hash = HashFunction(k, this->size);
		//	if (this->Table[Hash].Keys == "") {
		//		this->Table[Hash].Student = v;
		//		this->Table[Hash].Keys = k;
		//		this->Table[Hash].Next = NULL;
		//		return true;
		//	}
		//	else
		//	{
		//		Item Help = this->Table[Hash];
		//		while (Help.Next != NULL)
		//			Help = *Help.Next;
		//		Item *Element;
		//		Element = new Item;
		//		(*Element).Student = v;
		//		(*Element).Keys = k;
		//		(*Element).Next = NULL;
		//		Help.Next = Element;
		//		delete Element;
		//		return true;
		//	}
		//}
		//else {
		//	cout << "����� \n";
		//	//this->size = INIT_SIZE;
		//	//this->Table = new Item[this->size];

		//	//for (int i = 0; i < this->size; i++) { // <= 
		//	//	this->Table[i].Student = { 0, 0 };
		//	//	this->Table[i].Keys = "";
		//	//	this->Table[i].Next = NULL;
		//	//}

		//	Hash = HashFunction(k, this->size);
		//	this->Table[Hash].Student = v;
		//	this->Table[Hash].Keys = k;
		//	this->Table[Hash].Next = NULL;
		//	return true;
		//}
	};
	//���������� �������� �� �����. ������������ ����� 
	Value& operator[](const Key& k) {
		DEBUG && cout << "operator[] HashTable\n";
		unsigned int Hash = HashFunction(k, this->size);
		if (this->Table[Hash].Keys == k)
			return this->Table[Hash].Student;
		else {
			Item Help = this->Table[Hash];
			while (Help.Keys != k)
				Help = *(Help.Next);
			if (Help.Keys == k)
				return Help.Student;
			else
				cout << "I can't do it";
			//��� ������� ���� ��� ������ ��������???
		}
	};;
	//����������� �������� �� �����
	const Value& operator[](const Key& k) const {
		DEBUG && cout << "operator[] HashTable\n";
		unsigned int Hash = HashFunction(k, this->size);
		if (this->Table[Hash].Keys == k)
			return this->Table[Hash].Student;
		else {
			Item Help = this->Table[Hash];
			while (Help.Keys != k)
				Help = *(Help.Next);
			if (Help.Keys == k)
				return Help.Student;
			else
				cout << "I can't do it";
			//���� ����
		}
	};
	//���������� �������� �� �����. ������� ���������� ��� �������
	Value& at(const Key& k) {
		DEBUG && cout << "at HashTable\n";
		unsigned int Hash = HashFunction(k, this->size);
		try {
			if (this->Table[Hash].Keys == k)
				return this->Table[Hash].Student;
			else {
				Item Help = this->Table[Hash];
				while (Help.Keys != k)
					Help = *(Help.Next);
				if (Help.Keys == k)
					return Help.Student;
				else
					throw 123;
			}
		}
		catch (int i) {
			cout << "I can't do it. catch " << i << "\n";
		}
	};
	//���������� �������� �� �����. ������� ���������� ��� �������
	const Value& at(const Key& k) const {
		DEBUG && cout << "at HashTable\n";
		unsigned int Hash = HashFunction(k, this->size);
		try {
			if (this->Table[Hash].Keys == k)
				return this->Table[Hash].Student;
			else {
				Item Help = this->Table[Hash];
				while (Help.Keys != k)
					Help = *(Help.Next);
				if (Help.Keys ==
					k)
					return Help.Student;
				else
					throw 123;
			}
		}
		catch (int i) {
			cout << "I can't do it. catch " << i << "\n";
		}
	};
	//�������� �� �������
	bool empty() const {
		DEBUG && cout << "empty HashTable\n";

		for (unsigned int j = 0; j <= this->size; j++)
			if (this->Table[j].Keys != "") {
				return false;
			}
		return true;
	};
	//���������� ���-�� �������� � ����� �������
	size_t sizeTable() const {
		DEBUG && cout << "sizeTable HashTable\n";
		unsigned int Result = 0, i;
		for (i = 0; i <= this->size; i++)
			if (this->Table[i].Keys != "")
				Result++;
		return Result;
	};
	//����� �������� ���-�������
	void Print() const {
		DEBUG && cout << "Print HashTable\n";
		for (unsigned int i = 0; i <= this->size; i++) {
			if (this->Table[i].Keys != "") {
				cout << "Studet's name: " << Table[i].Keys << "\n";
				cout << "Age: " << Table[i].Student.age << "\n";
				cout << "Weight: " << Table[i].Student.weight << "\n\n";
				Item Help = this->Table[i];
				while (Help.Next != NULL) {
					cout << "Studet's name: " << Help.Keys << "\n";
					cout << "Age: " << Help.Student.age << "\n";
					cout << "Weight: " << Help.Student.weight << "\n\n";
					Help = *(Help.Next);
				}
			}
		}
	};


	// ����� �������
	Item* getPtrItemFromTable(int index)
	{
		if (ptrItemTable == NULL)
			return NULL; // ����� ������� ������ !

		if (index < 0 || index > this->size)
			return NULL; // ����� ������� ������ !

		return ptrItemTable[index];
	}

	// ����� �������
	// ������� �� ������� ��� �������� ��������(��. Insert)
	bool setPtrItemToTable(int index, Item* ptrItem)
	{
		if (ptrItem == NULL)
			return false;

		if (index < 0 || index > this->size)
			return false;

		ptrItemTable[index] = ptrItem;

		return true;
	}
};

//���-�������
unsigned int HashFunction(Key b, unsigned int s) {
	DEBUG && cout << "HashFunction HashTable\n";
	unsigned int j, h = 0, result;
	int l;
	l = b.length();
	for (j = l - 1; j >= 1; j--) {
		h = h + (unsigned int)b[l - j];
	}
	result = h % s;
	return result;
};

//������ ���-�������
bool operator==(const HashTable & a, const HashTable & b) {
	DEBUG && cout << "operator== HashTable\n";
	
	if (a.size != b.size) 
		return false;

	//unsigned int j;
	for (int j = 0; j <= a.size; j++) {
		if ((a.Table[j].Student.age != b.Table[j].Student.age) || (a.Table[j].Student.weight != b.Table[j].Student.weight) || (a.Table[j].Keys != b.Table[j].Keys) || (a.Table[j].Next != b.Table[j].Next)) {
			return false;
		}
		if (a.Table[j].Next != NULL) {
			Item Help = *(a.Table[j].Next);
			Item Helpb = *(b.Table[j].Next);
			while (Help.Next != NULL) {
				if ((Help.Keys != Helpb.Keys) || (Help.Student.age != Helpb.Student.age) || (Help.Student.weight != Helpb.Student.weight))
					return false;
				else
				{
					Help = *(Help.Next);
					Helpb = *(Helpb.Next);
				}
			}
		}
	}
	return true;
};

//�� ������ ���-�������
bool operator!=(const HashTable & a, const HashTable & b) {
	DEBUG && cout << "operator!= HashTable\n";
	unsigned int j;
	if (a.size == b.size)
		for (j = 0; j <= a.size; j++) {
			if ((a.Table[j].Student.age == b.Table[j].Student.age) && (a.Table[j].Student.weight == b.Table[j].Student.weight) && (a.Table[j].Keys == b.Table[j].Keys) && (a.Table[j].Next == b.Table[j].Next)) {
				if (a.Table[j].Next != NULL) {
					Item Help = *(a.Table[j].Next);
					Item Helpb = *(b.Table[j].Next);
					while (Help.Next != NULL) {
						if ((Help.Keys == Helpb.Keys) && (Help.Student.age == Helpb.Student.age) && (Help.Student.weight == Helpb.Student.weight))
							return false;
						else
						{
							Help = *(Help.Next);
							Helpb = *(Helpb.Next);
						}
					}
				}
			}
			return false;
		}
	return true;
};

std::ostream &operator<<(std::ostream &os, HashTable const &ht) {
	cout << "output \n";
	//if (ht.Table == NULL)
	//{
	//	DEBUG && cout << "operator<< HashTable don't init\n";
	//	return os;
	//}
		

	for (unsigned int i = 0; i <= ht.size; i++) {
		if (ht.Table[i].Keys != "") {
			os << ht.Table[i] << "\n";

			/*Item Help = ht.Table[i];
			while (Help.Next != NULL) {
				cout << "Studet's name: " << Help.Keys << "\n";
				cout << "Age: " << Help.Student.age << "\n";
				cout << "Weight: " << Help.Student.weight << "\n\n";
				Help = *(Help.Next);
			}

			Item* ptrHelp = &(ht.Table[i]);
			do
			{
				std::string valueNext = ptrHelp->Next == NULL ? " NULL\n " : " Item ->\n ";
				os << ptrHelp->Student << " keys= " << ptrHelp->Keys << " next= " << valueNext + offset;
				ptrHelp = ptrHelp->Next;
				offset += " ";
			} while (ptrHelp != NULL);*/
		}
	}

	return os << "\n";
}