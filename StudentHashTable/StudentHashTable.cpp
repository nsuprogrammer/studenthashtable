// StudentHashTable.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <iterator> // reverse

#include "HashTable.h"

bool testValue(void)
{
	cout << "testValue() \n";

	Value v1;
	cout << v1 << "\n";

	Value* ptr_v2 = new Value();

	(v1 == *ptr_v2) && cout << "v1 == *ptr_v2 \n";

	ptr_v2->age = 20;
	cout << *ptr_v2 << "\n";

	!(v1 == *ptr_v2) && cout << "v1 != *ptr_v2 \n";

	Value v3 = *ptr_v2;
	cout << v3 << "\n";

	(v3 == *ptr_v2) && cout << "v3 == *ptr_v2 \n";

	Value v4;
	cout << v4 << "\n";
	Value v5;
	cout << v5 << "\n";

	return true;
}

bool testItemDeepInner(void)
{
	cout << "testItemDeepInner() \n";

	//Item item(0, 0, NULL);
	//Item* ptrItem = &item;
	Item* ptrItem = new Item(0, 0, NULL);
	int numberItem = 20;
	for (int i = 1; i < numberItem; i++)
	{
		ptrItem = new Item(i, i, ptrItem);
	}

	cout << *ptrItem << "\n";

	Item::recursiveDeleteItemList(ptrItem);

	return true;
}

bool testItem(void)
{
	cout << "testItem() \n";

	Item item1(1, 1, NULL);
	Item item2(2, 2, &item1);
	Item item3(3, 3, &item2);
	cout << "item1: \n" << item1 << "\n";
	cout << "item2: \n" << item2 << "\n";
	cout << "item3: \n" << item3 << "\n";
	cout << item1.toString() << " " << item2.toString() << "\n";

	Item item22 = item2;
	item2.Next = NULL;
	cout << "item22: \n" << item22 << "\n";

	return true;
}

bool teslItemTable()
{
	cout << "teslItemTable() \n";

	int size = 20;
	Item** ptrItemTable = new Item*[size];
	for (int i = 0; i < 20; i++)
	{
		ptrItemTable[i] = NULL;
	}

	Item* ptrItem = new Item(001, 001);
	ptrItemTable[1] = ptrItem;

	ptrItemTable[3] = new Item(301, 301);
	ptrItemTable[4] = new Item(401, 401);

	ptrItemTable[6] = new Item(602, 602, new Item(601, 601));
	ptrItemTable[8] = new Item(803, 803, new Item(802, 802, new Item(801, 801, NULL)));


	for (int i = 0; i < size; i++)
	{
		cout << *ptrItemTable[i] << "\n";
	}

	//
	cout << "������ swap (��� �����������)\n";
	Item** newPtrItemTable = new Item*[size];
	for (int i = 0; i < size; i++)
	{
		newPtrItemTable[i] = ptrItemTable[size - i - 1];
	}

	for (int i = 0; i < size; i++)
	{
		cout << *newPtrItemTable[i] << "\n";
	}


	//
	/*
	cout << "����������� ������� (������ ������)\n";
	Item* ptrHelp = NULL;
	for (int i = 0; i < size/2; i++)
	{
		ptrHelp = ptrItemTable[size-i-1];
		ptrItemTable[size - i - 1] = ptrItemTable[i];
		ptrItemTable[i] = ptrHelp;
	}

	for (int i = 0; i < size; i++)
	{
		cout << *ptrItemTable[i] << "\n";
	}
	*/


	// ������� ������� ����� ������ ���� ��� 
	// ��� ���� ������ �� ������� ���������� � ���� �������� - ������� ����
	// 
	// 
	for (int i = 0; i < size; i++)
	{
		// delete newPtrItemTable[i]; // �� ������ ��������� ������� 
		Item::recursiveDeleteItemList(newPtrItemTable[i]);
	}

	// ������ ����� ������� ���� ������� ���������� 
	// ������ ��� �� ��� !
	delete[] ptrItemTable;
	delete[] newPtrItemTable;

	return true;
}

bool testHashTable()
{
	cout << "testHashTable() \n";

	HashTable* ptrHT = new HashTable();
	//cout << *ptrHT << "\n";

	//ptrHT->insert((Key)"one", *(new Value()));
	
	
	HashTable* ht1 = new HashTable();
	ht1->Print();
	delete ht1;

	HashTable ht2;
	HashTable ht3;
	//(ht2 != ht3) && cout << "ht2 == ht3 \n";

	ht2.Print();

	return true;
}

void testAll(void)
{
	cout << "testAll() \n";

	//testValue();
	//testItem();
	//testItemDeepInner();
	teslItemTable();
	//testHashTable();

	
}


int main()
{
	setlocale(LC_ALL, "RUS");
	std::cout << "StudentHashTable v1-2 \n";

	testAll();

	std::cin.get();
    return 0;
}

